import java.util.Scanner;
public class MethodArray {
	public static void main(String args[]) {
		Scanner keyboard = new Scanner(System.in);
		int sum = 0;
		System.out.print("Input the number of the elements you want to put in the array: ");
		int n = keyboard.nextInt();
		
		int arr[] = new int[n];
		int temp;
		
		for (int i = 0; i < n; i++) {
			System.out.print("Input the " + (i+1) + "element of the array: ");
			arr[i] = keyboard.nextInt();
		}
		
		for (int i = 0; i < n; i++) {
			for (int j = i+1; j < n; j++) {
				if (arr[i] > arr[j]) {
					temp = arr[i];
					arr[i] = arr[j];
					arr[j] = temp;
				} 
			}
		}
		
		for (int i = 0; i < n; i++) {
	        sum += arr[i];
	        System.out.print(arr[i] + "\t");
	    }
		System.out.println(" ");
		
		double ave = (double)sum/n;
		
		System.out.println("Sum of the array is: " + sum);
		System.out.println("Average of the int array is: " + ave);
		
		System.exit(0);
	}
}
