import java.util.Scanner;
public class MatrixMethod {
	public static void main(String args[]) {
		Scanner keyboard = new Scanner(System.in);
		int n1, n2, m1, m2, i, j;
		
		
		System.out.println("Matrix A");
		do
		{
			System.out.print("Input the number of line n1: ");
			n1 = keyboard.nextInt();
			System.out.print("Input the number of column n2: ");
			n2 = keyboard.nextInt();
			if (n1 <= 0 || n2 <= 0) System.out.println("n1, n2 must be an postive integer");
		} while (n1 <= 0 || n2 <= 0);
		int A[][] = new int[n1][n2];
		for (i = 0; i < n1; i++) {
			System.out.println("Input value for the columen " + (i+1));
			for (j = 0; j < n2; j++) {
				System.out.print("A[" + (i+1) + "][" + (j+1) + "] = ");
				A[i][j] = keyboard.nextInt();
			}
		}
		System.out.print("\n");
		
		
		System.out.println("Matrix B");
		do
		{
			System.out.print("Input the number of line m1: ");
			m1 = keyboard.nextInt();
			System.out.print("Input the number of column m2: ");
			m2 = keyboard.nextInt();
			if (m1 <= 0 || m2 <= 0) System.out.println("m1, m2 must be an postive integer");
		} while (m1 <= 0 || m2 <= 0);
		int B[][] = new int[m1][m2];
		for (i = 0; i < m1; i++) {
			System.out.println("Input value for the columen " + (i+1));
			for (j = 0; j < m2; j++) {
				System.out.print("B[" + (i+1) + "][" + (j+1) + "] = ");
				B[i][j] = keyboard.nextInt();
			}
		}
		System.out.println(" ");
		
		if (n1 != m1 || n2 != m2) System.out.println("Two matrix don't have same size. Cannot compute sum of them.");
		else {
			System.out.println("The sum matrix of matrix A and matrix B is: ");
			for (i = 0; i < n1; i++) {
				for (j = 0; j < n2; j++) {
					System.out.print((A[i][j] + B[i][j]) + "\t");
				}
				System.out.println();
			}
		}
		
		System.exit(0);
	}
}
