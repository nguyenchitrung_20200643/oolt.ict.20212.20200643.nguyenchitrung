import java.util.Scanner;
import java.util.ArrayList;

public class CheckMonth {
	public static void main(String args[]) {
		Scanner keyboard = new Scanner(System.in);

		String[] month1 = {"January", "February", "March", "April", "May", "June", 
							"July", "August", "September", "October", "November", "December"};
		
		String[] month2 = {"Jan.", "Feb.", "Mar.", "Apr.", "May", "June", 
							"July", "Aug.", "Sept", "Oct.", "Nov.", "Dec."};
		
		String[] month3 = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", 
							"Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
		
		String[] month4 = {"1", "2", "3", "4", "5", "6", 
						 	"7", "8", "9", "10", "11", "12"};
						 	
		int i = 0, j = -1;
		int[] numberofDay = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
		int year = 1800;

		String month;
		
		do {
			System.out.print("Input month: ");
			month = keyboard.nextLine();
			
			for (i = 0; i < 12; i++) {
				if (month.equals(month1[i])) {
					j = i;
				}
				if (month.equals(month2[i])) {
					j = i;
				}
				if (month.equals(month3[i])) {
					j = i;
				}
				if (month.equals(month4[i])) {
					j = i;
				}
			}

			if (j == -1)
				System.out.println("You must input month (1-12) in one of four ways(full name, abbreviation, 3 letters, number)");

		} while (j == -1);	
	
		do {
			System.out.print("Input year: ");
			year = keyboard.nextInt();
			if (year < 1) System.out.println("Year must be a positive integer");
		} while (year < 1);
		
		if (j != 1) 
			System.out.println(month1[j] + " " + year + " has " + numberofDay[j] + " days");
		else {
			if (year % 4 != 0) 
				System.out.println(month1[j] + " " + year + " has " + numberofDay[j] + " days");
			else {
				if (year % 100 == 0 && year % 400 != 0) {
					System.out.println(month1[j] + " " + year + " has " + numberofDay[j] + " days");
				}
				else {
					System.out.println(month1[j] + " " + year + " has " + (numberofDay[j]+1) + " days");
				}
					
			}
		}
		System.exit(0);
	}
}
