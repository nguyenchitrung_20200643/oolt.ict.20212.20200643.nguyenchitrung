import java.util.Scanner;
public class MakeTriangle {
	public static void main(String args[]) {
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("Input argument n for display a triangle");
		System.out.print("(Triangle will has edge with a height of n star *): ");
		int n = keyboard.nextInt();
		
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n-1 - i; j++) {
				System.out.print(" ");
			}
			
			for (int j = 0; j < 2*i+1; j++) {
				System.out.print("*");
			}
			System.out.println(" ");
		}
		
		System.exit(0);
		
	}
}
