
public class Order {
	public static final int MAX_NUMBER_ORDERED = 10;
	
	private DigitalVideoDisc itemsOrdered[] = new DigitalVideoDisc[MAX_NUMBER_ORDERED];
	
	int qtyOrdered;
	
	void addDigitalVideoDisc(DigitalVideoDisc disc) {
		if (qtyOrdered == 10)
			System.out.println("Cannot add anymore items");
		else {
			qtyOrdered = qtyOrdered + 1;
			itemsOrdered[qtyOrdered - 1] = disc;
		}
	}
	
	void removeDigitalVideoDisc(DigitalVideoDisc disc) {
		if (qtyOrdered == 0) System.out.println("Empty of items");
		else 
		{
			for (int i = 0; i < qtyOrdered; i++) 
				if (itemsOrdered[i] == disc) {
					qtyOrdered = qtyOrdered - 1;
					itemsOrdered[i] = null;
				}
		}
	}
	
	
	double totalCost() {
		int i;
		double sum = 0.0;
		
		for (i = 0; i < 10; i++)
			if (itemsOrdered[i] != null) sum = sum + itemsOrdered[i].getCost();
		return sum;
	}
}
