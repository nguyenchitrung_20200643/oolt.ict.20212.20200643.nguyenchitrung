
public class DateTest {
	public static void main (String[] args)
	{
		MyDate day1 = new MyDate();
		day1.printDay();
		
		day1.accept("September 21st 2022");
		day1.printDay();
		
		MyDate day2 = new MyDate(1, 3, 2020);
		day2.printDay();
		
		MyDate day3 = new MyDate("April 23rd 2019");
		day3.printDay();
		
		System.exit(0);
	}	
}