import java.util.Scanner;
import java.lang.Math;

public class SolveEqn {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
		boolean cont = true;
		
        do {
            System.out.println("===========================MENU===========================");
            System.out.println("1. Solve system (two first degree equations withe one var)");
            System.out.println("2. Solve second degree equation with one variable");
            System.out.println("3. Exit");
            
            System.out.print("Choose: ");
            int option = input.nextInt();
            
            switch (option) {
                case 1:
                {
                    System.out.println("a11x + a12y = b1");
                    System.out.println("a21x + a22y = b2");

                    System.out.print("Enter a11 = ");
                    var a11 = input.nextDouble();

                    System.out.print("Enter a12 = ");
                    var a12 = input.nextDouble();

                    System.out.print("Enter b1 = ");
                    var b1 = input.nextDouble();
                    
                    System.out.print("Enter a21 = ");
                    var a21 = input.nextDouble();
                    
                    System.out.print("Enter a22 = ");
                    var a22 = input.nextDouble();
                    
                    System.out.print("Enter b2 = ");
                    var b2 = input.nextDouble();

                    var D = a11*a22 - a21*a12;
                    var Dx = b1*a22 - b2*a12;
                    var Dy = a11*b2 - a21*b1;

                    if (D != 0){
                        System.out.println("The system has a unique solution (x, y) = (" + Dx/D + ", " + Dy/D + ")");
                    } else if (D == 0 && Dx == 0 && Dy ==0) {
                        System.out.println("The system has infinite solutions");
                    } else {
                        System.out.println("The system has no solution");
                    }
                    

                }
                    break;
                
                case 2:
                {
					System.out.println("ax^2 + bx + c = 0");
					
					double a;
					do
					{
                    	System.out.print("Enter a = ");
                    	a = input.nextDouble();
                    	if (a == 0) System.out.println("The second degree equation must have non-zero coefficient a");
                    	else break;
					} while (true);
					
                    System.out.print("Enter b = ");
                    var b = input.nextDouble();

                    System.out.print("Enter c = ");
                    var c = input.nextDouble();
                    
                    var delta = b * b - 4 * a * c;
                    
                    if (delta < 0) System.out.println("The equation has no solution");
                    else if (delta == 0) System.out.println("The equation has one solution x = " + -b/(2*a));
                    else{
                 		var x1 = (-b + Math.sqrt(delta)) / (2*a);
                 		var x2 = (-b - Math.sqrt(delta)) / (2*a);
                 		System.out.println("The equation has two solutions");
                 		System.out.println("x1 = " + x1);
                 		System.out.println("x2 = " + x2);
                 	}
                    
                }
                    break;
                
                case 3:
                	cont = false;
                    break;
                    
                default:
                    System.out.println("You must choose from 1 to 3");
            }
        } while (cont);

        System.exit(0);
    }
}
