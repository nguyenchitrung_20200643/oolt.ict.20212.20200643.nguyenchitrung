import java.util.Scanner;
public class Calculator {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        System.out.print("Enter the first number: ");
        var num1 = input.nextDouble();
        System.out.println("The first number which you entered is: " + num1);

        System.out.print("Enter the second number: ");
        var num2 = input.nextDouble();
        System.out.println("The first number which you entered is: " + num2);

        var sum = num1 + num2;
        System.out.println("num1 + num2 = " + sum);
        
        var diff = num1 - num2;
        System.out.println("num1 - num2 = " + diff);

        var product = num1 * num2;
        System.out.println("num1 x num2 = " + product);

        if (num2 == 0f) {
            System.out.println("num2 = 0, so num1 can not be devide by num2");
        } else {
            var quotient = num1 / num2;
            System.out.println("num1 : num2 = " + quotient);
        }

        System.exit(0);
    }
}   
