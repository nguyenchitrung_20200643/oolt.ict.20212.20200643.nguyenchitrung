package hust.soict.globalict.aims;

import hust.soict.globalict.aims.disc.DigitalVideoDisc;
import hust.soict.globalict.aims.order.Order;

public class Aims {
	
	public static void main(String[] args) {
		Order anOrder = new Order();
		
		DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King");
		dvd1.setCategory("Animation");
		dvd1.setCost(19.95f);
		dvd1.setDirector("George Lucas");
		dvd1.setLength(87);
		anOrder.addDigitalVideoDisc(dvd1);
		
		DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star Wars");
		dvd2.setCategory("Science Fiction");
		dvd2.setCost(24.95f);
		dvd2.setDirector("George Lucas");
		dvd2.setLength(124);
		anOrder.addDigitalVideoDisc(dvd2);
		
		DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladdin");
		dvd3.setCategory("Cartion");
		dvd3.setCost(18.99f);
		dvd3.setDirector("George Lucas");
		dvd3.setLength(110);
		anOrder.addDigitalVideoDisc(dvd3);
		
		anOrder.displayOrder();
		
		Order anOrder1 = new Order();
		
		anOrder1.addDigitalVideoDisc(dvd3);
		anOrder1.addDigitalVideoDisc(dvd2);
		anOrder1.addDigitalVideoDisc(dvd1);
		
		System.out.println();
		anOrder1.displayOrder();
		
		System.exit(0);
	}
}
