package hust.soict.globalict.aims.utils;
import java.time.LocalDate; 
public class MyDate {
	private int day;
	private int month;
	private int year;
	
	public int getDay() {
		return day;
	}
	public void setDay(int day) {
		this.day = day;
	}
	public int getMonth() {
		return month;
	}
	public void setMonth(int month) {
		this.month = month;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	
	public MyDate() {
		LocalDate myObj = LocalDate.now();
		this.day = myObj.getDayOfMonth();
		this.month = myObj.getMonthValue();
		this.year = myObj.getYear();
	}
	public MyDate(int day, int month, int year) {
		this.day = day;
		this.month = month;
		this.year = year;
	}
	public MyDate(String myDay)
	{
		String[] arrOfStr = myDay.split(" ");
		String[] month1 = {"January", "February", "March", "April", "May", "June", 
				"July", "August", "September", "October", "November", "December"};
		String[] day1 = {"1st", "2nd", "3rd", "4th", "5th", "6th", "7th", "8th",
				"9th", "10th", "11th", "12th", "13th", "14th", "15th", "16th",
				"17th", "18th", "19th", "20th", "21st", "22nd", "23rd", "24th",
				"25th", "26th", "27th", "28th", "29th", "30th", "31th"};
		int i;
		
		for (i = 0; i < 12; i++) {
			if (arrOfStr[0].equals(month1[i])) {
				this.month = i+1; 
			}
		}
		for (i = 0; i < 31; i++) {
			if (arrOfStr[1].equals(day1[i])) this.day = i+1;
		}
		this.year = Integer.parseInt(arrOfStr[2]);
	}
	
	public void accept(String myDay) {
		String[] arrOfStr = myDay.split(" ");
		String[] month1 = {"January", "February", "March", "April", "May", "June", 
				"July", "August", "September", "October", "November", "December"};
		String[] day1 = {"1st", "2nd", "3rd", "4th", "5th", "6th", "7th", "8th",
				"9th", "10th", "11th", "12th", "13th", "14th", "15th", "16th",
				"17th", "18th", "19th", "20th", "21st", "22nd", "23rd", "24th",
				"25th", "26th", "27th", "28th", "29th", "30th", "31th"};
		int i;
		
		for (i = 0; i < 12; i++) {
			if (arrOfStr[0].equals(month1[i])) {
				setMonth(i+1); 
			}
		}
		for (i = 0; i < 31; i++) {
			if (arrOfStr[1].equals(day1[i])) setDay(i+1);
		}
		setYear(Integer.parseInt(arrOfStr[2]));
	}
	
	public void printDay() {
		System.out.println("The current day is: " + day + "-" + month + "-" + year);
	}
}