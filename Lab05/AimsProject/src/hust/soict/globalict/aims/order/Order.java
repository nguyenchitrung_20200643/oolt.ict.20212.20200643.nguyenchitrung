package hust.soict.globalict.aims.order;
import hust.soict.globalict.aims.disc.DigitalVideoDisc;
import hust.soict.globalict.aims.utils.MyDate;

public class Order {
	public static final int MAX_NUMBER_ORDERED = 10;
	
	private DigitalVideoDisc itemsOrdered[] = new DigitalVideoDisc[MAX_NUMBER_ORDERED];
	private int qtyOrdered = 0;
	private MyDate dateOrdered;
	
	public static final int MAX_LIMITTED_ORDERS = 5;
	private static int nbOrders = 0;
	private int luckyItem = 0;
	
	public Order() {
		if (nbOrders == MAX_LIMITTED_ORDERS) {
			System.out.println("Out of allowed orders!");
			return;
		}
		nbOrders++;
		this.dateOrdered = new MyDate();
	}

	public int getQtyOrdered() {
		return qtyOrdered;
	}

	public void setQtyOrdered(int qtyOrdered) {
		this.qtyOrdered = qtyOrdered;
	}

	public void addDigitalVideoDisc(DigitalVideoDisc disc) {
		if (qtyOrdered == 10)
			System.out.println("Cannot add anymore items");
		else {
			qtyOrdered = qtyOrdered + 1;
			itemsOrdered[qtyOrdered - 1] = disc;
		}
	}
	
	void addDigitalVideoDisc(DigitalVideoDisc[] dvdList) {
		if (qtyOrdered == 10)
			System.out.println("Cannot add anymore items");
		else if (qtyOrdered + dvdList.length > 10)
			System.out.println("Not enough space");
		else {
			for (int i = 0; i < dvdList.length; i++)
			{
				itemsOrdered[qtyOrdered-1+i] = dvdList[i];
			}
			qtyOrdered = qtyOrdered + dvdList.length;
		}
	}
	
	void addDigitalVideoDisc(DigitalVideoDisc disc1, DigitalVideoDisc disc2) {
		if (qtyOrdered == 10)
			System.out.println("Cannot add anymore items");
		else {
			itemsOrdered[qtyOrdered - 1] = disc1;
			itemsOrdered[qtyOrdered] = disc2;
			qtyOrdered = qtyOrdered + 2;
		}	
	}
	
	void removeDigitalVideoDisc(DigitalVideoDisc disc) {
		if (qtyOrdered == 0) System.out.println("Empty of items");
		else 
		{
			for (int i = 0; i < qtyOrdered; i++) 
				if (itemsOrdered[i] == disc) {
					qtyOrdered = qtyOrdered - 1;
					itemsOrdered[i] = null;
				}
		}
	}
	
	
	double totalCost() {
		int i;
		double sum = 0.0;
		
		for (i = 0; i < 10; i++)
			if (itemsOrdered[i] != null) sum = sum + itemsOrdered[i].getCost();
		return sum;
	}
	
	public void displayOrder() {
		System.out.println("***********************Order***********************");
		System.out.print("Date: ");
		dateOrdered.printDay();
		
		System.out.println("Ordered Items");
		for (int i = 0; i < qtyOrdered; i++) {
			System.out.println((i + 1) + ". DVD - " + itemsOrdered[i].getTitle() + " - " + itemsOrdered[i].getCategory()
                    + " - " + itemsOrdered[i].getDirector() + " - " + itemsOrdered[i].getLength() + ": "
                    + itemsOrdered[i].getCost() + "$");
        }
        System.out.printf("Total cost: %.2f$\n", totalCost());
        System.out.println("***************************************************");
		
	}
	
	public void getALuckyItem( ) {
		if (luckyItem == 1) { 
			System.out.println("You had a lucky item");
		}
		else {
			int tmp = (int) ((Math.random()) * ((qtyOrdered - 0) + 1));
			itemsOrdered[tmp].setCost(0f);
			System.out.println("Lucky item is: " + itemsOrdered[tmp].getTitle());
			luckyItem++;
		}
	}
}
