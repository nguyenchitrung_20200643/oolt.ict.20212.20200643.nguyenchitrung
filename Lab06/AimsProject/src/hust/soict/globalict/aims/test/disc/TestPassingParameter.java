package hust.soict.globalict.aims.test.disc;
import hust.soict.globalict.aims.disc.DigitalVideoDisc;

public class TestPassingParameter {

	public static void main(String[] args) {
		DigitalVideoDisc jungleDVD = new DigitalVideoDisc("Jungle");
		DigitalVideoDisc cinderellaDVD = new DigitalVideoDisc("Cinderella");
		
		cinderellaDVD = swap(jungleDVD, jungleDVD = cinderellaDVD);
		System.out.println("Jungle dvd title: " + jungleDVD.getTitle());
		System.out.println("Cinderella dvd title: " + cinderellaDVD.getTitle());
		
	}
	
	public static DigitalVideoDisc swap(DigitalVideoDisc o1, DigitalVideoDisc o2)
	{
		return o1;
	}
	
	public static void changeTitle(DigitalVideoDisc dvd, String title) {
		String oldTitle = dvd.getTitle();
		dvd.setTitle(title);
		dvd = new DigitalVideoDisc(oldTitle);
		System.out.println("DVD title: " + dvd.getTitle());
	}

}
