package hust.soict.globalict.aims.media;

import java.util.*;

public class Book extends Media{
	private List<String> authors = new ArrayList<String>();
	
	public Book(String title, String category, List<String> authors) {
		super(title, category);
		this.authors = authors;
		
		//TODO: check author condition
	}

	public List<String> getAuthors() {
		return authors;
	}

	public void setAuthors(List<String> authors) {
		this.authors = authors;
	}
	
	public void addAuthor(String authorName) {
		authors.add(authorName);
	}
	
	public void removeAuthor(String authorName) {
		if(authors.isEmpty()) System.out.println("Authors list is empty");
		else {
			if (!authors.contains(authorName)) {
				System.out.println("Authors list doesn't have the name "+ authorName);
			}
			else {
				authors.remove(authorName);
			}
		}
	}
}
