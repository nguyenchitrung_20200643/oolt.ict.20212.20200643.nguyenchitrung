package hust.soict.globalict.aims.disc;

import hust.soict.globalict.aims.media.Media;

public class DigitalVideoDisc extends Media{
	private String director;
	private int length;
	
	public String getDirector() {
		return director;
	}
	public void setDirector(String director) {
		this.director = director;
	}
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
	public DigitalVideoDisc(String title, String category, String director, int length, float cost) {
        super();
        this.title = title;
        this.category = category;
        this.director = director;
        this.length = length;
        this.cost = cost;
    }

    public DigitalVideoDisc(String title, String category, String director) {
        super();
        this.title = title;
        this.category = category;
        this.director = director;
    }

    public DigitalVideoDisc(String title, String category) {
        super();
        this.title = title;
        this.category = category;
    }

    public DigitalVideoDisc(String title) {
        super();
        this.title = title;
    }
	public boolean seach(String title)
	{
		return this.title.equals(title);
	}
	
}
