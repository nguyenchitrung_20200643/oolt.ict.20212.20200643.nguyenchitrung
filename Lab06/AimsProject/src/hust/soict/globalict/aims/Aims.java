package hust.soict.globalict.aims;

import hust.soict.globalict.aims.media.Media;
import hust.soict.globalict.aims.order.Order;
import java.util.Scanner;

public class Aims {
	public static void showMenu() {
		System.out.println("Order Management Application: ");
		System.out.println("--------------------------------");
		System.out.println("1. Create new order");
		System.out.println("2. Add item to the order");
		System.out.println("3. Delete item by id");
		System.out.println("4. Display the items list of order");
		System.out.println("0. Exit");
		System.out.println("--------------------------------");
		System.out.println("Please choose a number: 0-1-2-3-4");
	}
	
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		Order order = null;
		int id = 0;
		while (true) {
			showMenu();
			int option = scanner.nextInt();
			if (option == 0) break;
			switch (option) {
				case 1:
					order = new Order();
					order.setId(id);
					id += 1;
					break;
				case 2:
					if (order == null) {
						System.out.println("No order found");
						break;
					}
					System.out.print("Item title: ");
					String title = scanner.next();
					System.out.print("Item category: ");
					String category = scanner.next();
					System.out.print("Item cost: ");
					float cost = scanner.nextFloat();
					Media media = new Media(title, category);
					media.setCost(cost);
					media.setId(id);
					id += 1;
					order.addMedia(media);
					break;
				case 3:
					if (order == null) {
						System.out.println("No order found");
						break;
					}
					System.out.print("Remove item id: ");
					order.removeMedia(scanner.nextInt());
					break;
				case 4:
					if (order == null) {
						System.out.println("No order found");
						break;
					}
					order.print();
					break;
				default:
					break;
			}
		}
		scanner.close();
	}
}
