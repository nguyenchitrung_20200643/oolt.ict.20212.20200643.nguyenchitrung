package hust.soict.globalict.aims;
import hust.soict.globalict.aims.disc.DigitalVideoDisc;
import hust.soict.globalict.aims.order.*;

public class DiskTest {
	public static void main(String[] args) {
		DigitalVideoDisc dvd = new DigitalVideoDisc("Harry Porter");
		System.out.println("Search result: " + dvd.seach("  harry    porter  "));
		
		DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King", "Animation", "Roger Allers", 87, 19.95f);
		DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star Wars", "Science Fiction", "George Lucas", 124, 24.95f);
		DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladdin", "Animation", "John Musker", 90, 18.99f);
		
		Order order = new Order();
		order.addMedia(dvd1, dvd2, dvd3);
		order.getALuckyItem();
		order.print();		
	}
}