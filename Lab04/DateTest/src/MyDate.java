import java.time.LocalDate; 
public class MyDate {
	private int day;
	private int month;
	private int year;
	
	String[] month1 = {"January", "February", "March", "April", "May", "June", 
			"July", "August", "September", "October", "November", "December"};
	
	String[] day1 = {"1st", "2nd", "3rd", "4th", "5th", "6th", "7th", "8th",
			"9th", "10th", "11th", "12th", "13th", "14th", "15th", "16th",
			"17th", "18th", "19th", "20th", "21st", "22nd", "23rd", "24th",
			"25th", "26th", "27th", "28th", "29th", "30th", "31th"};
	
	String[] day2 = {"First", "Second", "Third", "Fourth", "Fifth", "Sixth", "Seventh",
			"Eight", "Nineth", "Tenth", "Eleventh", "Twelveth", "Thirteenth", "Fourteenth",
			"Fifteenth", "Sixteenth", "Seventeenth", "Eighteenth", "Nineteenth", "Twentieth",
			"Twenty First", "Twenty Second", "Twenty Third", "Twenty Fourth", "Twenty Fifth",
			"Twenty Sixth", "Twenty Seventh", "Twenty Eigth", "Twenty Nineth", "Thirtieth", "Thirty First"};

	String[] month3 = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", 
			"Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
	
	public int getDay() {
		return day;
	}
	public void setDay(int day) {
		this.day = day;
	}
	public int getMonth() {
		return month;
	}
	public void setMonth(int month) {
		this.month = month;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	
	public MyDate() {
		LocalDate myObj = LocalDate.now();
		this.day = myObj.getDayOfMonth();
		this.month = myObj.getMonthValue();
		this.year = myObj.getYear();
	}
	
	public MyDate(int day, int month, int year) {
		this.day = day;
		this.month = month;
		this.year = year;
	}
	
	public MyDate(String myDay)
	{
		String[] arrOfStr = myDay.split(" ");
		int i;
		
		for (i = 0; i < 12; i++) {
			if (arrOfStr[0].equals(month1[i])) {
				this.month = i+1; 
			}
		}
		for (i = 0; i < 31; i++) {
			if (arrOfStr[1].equals(day1[i])) this.day = i+1;
		}
		this.year = Integer.parseInt(arrOfStr[2]);
	}
	public MyDate(String day, String month, String year) {
		int i;
		
		for (i = 0; i < 12; i++) {
			if (day.equals(day1[i])) {
				this.day = i+1;
				break;
			}
			if (day.equals(day2[i])) {
				this.day = i+1;
				break;
			}
		}
	}
	
	public void accept(String myDay) {
		String[] arrOfStr = myDay.split(" ");
		int i;
		
		for (i = 0; i < 12; i++) {
			if (arrOfStr[0].equals(month1[i])) {
				setMonth(i+1); 
			}
		} 	
		for (i = 0; i < 31; i++) {
			if (arrOfStr[1].equals(day1[i])) setDay(i+1);
		}
		setYear(Integer.parseInt(arrOfStr[2]));
	}
	
	public void printDay(int a) {
		if (a == 1) {
			System.out.println(year + "-" + month + "-" + year);
		}
		
		if (a == 2) {
			System.out.println(day + "/" + month + "/" + year);
		}
		
		if (a == 3) {
			System.out.println(day + " " + month3[month-1] + " " + year);
		}
		
		if (a == 4) {
			System.out.println(month3[month-1] + " " + day + " " + year);
		}
		
		if (a == 5) {
			System.out.println(month + "-" + day + "-" + year);
		}	
	}
	
	
}