
public class DateTest {
	public static void main (String[] args)
	{
		MyDate[] dateArr = new MyDate[4];
		
		dateArr[0] = new MyDate(1, 5, 2020);
		dateArr[1] = new MyDate(4, 1, 2020);
		dateArr[2] = new MyDate(1, 1, 2019);
		dateArr[3] = new MyDate(4, 1, 2020);
		
		DateUtils.sortMyDateArr(dateArr);
		
		for (int i = 0; i < 4; i++) {
			dateArr[i].printDay(2);
		}
	}	
}