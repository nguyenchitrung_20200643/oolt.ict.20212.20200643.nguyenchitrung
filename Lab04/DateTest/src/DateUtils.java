

public class DateUtils {
	public static int myDateEquals(MyDate day1, MyDate day2) {
		int value = 0;
		if (day1.getYear() < day2.getYear()) {
			value = -1;
		}
		else if (day1.getYear() > day2.getYear()) {
			value = 1;
		}
		else {
			if (day1.getMonth() < day2.getMonth()) {
				value = -1;
			}
			else if (day1.getMonth() > day2.getMonth()) {
				value = 1;
			}
			else {
				if (day1.getDay() < day2.getDay()) {
					value = -1;
				}
				else if (day1.getDay() > day2.getDay()) {
					value = 1;
				}
			}
		}
		
		return value;
	}
	
	public static void sortMyDateArr(MyDate[] dateArr) {
		int i;
		int j;
		
		for (i = 0; i < dateArr.length-1; i++) {
			for (j = i+1; j < dateArr.length; j++) {
				if (myDateEquals(dateArr[i], dateArr[j]) > 0) {
					dateArr[j] = swap(dateArr[i], dateArr[i] = dateArr[j]);
				}
			}
		}
	}
	
	public static MyDate swap(MyDate day1, MyDate day2) {
		return day1;
	}
}


	
